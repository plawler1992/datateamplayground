﻿namespace ParameterGenerator
{
    partial class ParameterGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtParametersIn = new System.Windows.Forms.TextBox();
            this.lblParameters = new System.Windows.Forms.Label();
            this.txtParamOut = new System.Windows.Forms.TextBox();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.rbtnReport = new System.Windows.Forms.RadioButton();
            this.rbtnSubscription = new System.Windows.Forms.RadioButton();
            this.lblCopyFrom = new System.Windows.Forms.Label();
            this.chkQuotes = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtParametersIn
            // 
            this.txtParametersIn.Location = new System.Drawing.Point(88, 43);
            this.txtParametersIn.Name = "txtParametersIn";
            this.txtParametersIn.Size = new System.Drawing.Size(381, 20);
            this.txtParametersIn.TabIndex = 0;
            // 
            // lblParameters
            // 
            this.lblParameters.AutoSize = true;
            this.lblParameters.Location = new System.Drawing.Point(15, 43);
            this.lblParameters.Name = "lblParameters";
            this.lblParameters.Size = new System.Drawing.Size(67, 13);
            this.lblParameters.TabIndex = 1;
            this.lblParameters.Text = "Param String";
            // 
            // txtParamOut
            // 
            this.txtParamOut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtParamOut.Location = new System.Drawing.Point(13, 79);
            this.txtParamOut.Multiline = true;
            this.txtParamOut.Name = "txtParamOut";
            this.txtParamOut.Size = new System.Drawing.Size(638, 439);
            this.txtParamOut.TabIndex = 2;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(475, 43);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 3;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(557, 43);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // rbtnReport
            // 
            this.rbtnReport.AutoSize = true;
            this.rbtnReport.Checked = true;
            this.rbtnReport.Location = new System.Drawing.Point(18, 12);
            this.rbtnReport.Name = "rbtnReport";
            this.rbtnReport.Size = new System.Drawing.Size(57, 17);
            this.rbtnReport.TabIndex = 5;
            this.rbtnReport.TabStop = true;
            this.rbtnReport.Text = "Report";
            this.rbtnReport.UseVisualStyleBackColor = true;
            this.rbtnReport.CheckedChanged += new System.EventHandler(this.rbtnReport_CheckedChanged);
            // 
            // rbtnSubscription
            // 
            this.rbtnSubscription.AutoSize = true;
            this.rbtnSubscription.Location = new System.Drawing.Point(81, 12);
            this.rbtnSubscription.Name = "rbtnSubscription";
            this.rbtnSubscription.Size = new System.Drawing.Size(83, 17);
            this.rbtnSubscription.TabIndex = 6;
            this.rbtnSubscription.Text = "Subscription";
            this.rbtnSubscription.UseVisualStyleBackColor = true;
            this.rbtnSubscription.CheckedChanged += new System.EventHandler(this.rbtnSubscription_CheckedChanged);
            // 
            // lblCopyFrom
            // 
            this.lblCopyFrom.AutoSize = true;
            this.lblCopyFrom.Location = new System.Drawing.Point(170, 14);
            this.lblCopyFrom.Name = "lblCopyFrom";
            this.lblCopyFrom.Size = new System.Drawing.Size(255, 13);
            this.lblCopyFrom.TabIndex = 7;
            this.lblCopyFrom.Text = "Copy from ReportsServer.ExecutionLog2 Parameters";
            // 
            // chkQuotes
            // 
            this.chkQuotes.AutoSize = true;
            this.chkQuotes.Location = new System.Drawing.Point(487, 14);
            this.chkQuotes.Name = "chkQuotes";
            this.chkQuotes.Size = new System.Drawing.Size(82, 17);
            this.chkQuotes.TabIndex = 8;
            this.chkQuotes.Text = "Add Quotes";
            this.chkQuotes.UseVisualStyleBackColor = true;
            // 
            // ParameterGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 530);
            this.Controls.Add(this.chkQuotes);
            this.Controls.Add(this.lblCopyFrom);
            this.Controls.Add(this.rbtnSubscription);
            this.Controls.Add(this.rbtnReport);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.txtParamOut);
            this.Controls.Add(this.lblParameters);
            this.Controls.Add(this.txtParametersIn);
            this.Name = "ParameterGenerator";
            this.Text = "Parameter Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtParametersIn;
        private System.Windows.Forms.Label lblParameters;
        private System.Windows.Forms.TextBox txtParamOut;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.RadioButton rbtnReport;
        private System.Windows.Forms.RadioButton rbtnSubscription;
        private System.Windows.Forms.Label lblCopyFrom;
        private System.Windows.Forms.CheckBox chkQuotes;
    }
}

