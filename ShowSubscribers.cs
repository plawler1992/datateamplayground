﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace HelpfulToolsLauncher
{
    public partial class ShowSubscribers : Form
    {
        public ShowSubscribers()
        {
            InitializeComponent();
        }

        private void btnChooseFile_Click(object sender, EventArgs e)
        {
            if(ofdOpen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lblChosenFile.Text = ofdOpen.FileName;
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(ofdOpen.FileName);
                Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
                Excel.Range xlRange = xlWorksheet.UsedRange;

                Dictionary<string, int> subscribers = new Dictionary<string, int>();

                int paramCol = 6;
                for(int i = 2; i <= xlRange.Rows.Count; i++)
                {
                    string parameters = xlRange.Cells[i, paramCol].Value2.ToString();
                    parameters = formatSubscriptionString(parameters);
                    parameters = parameters.Split('^')[0];
                    string subscriber = parameters.Split('&')[1];
                    if (subscribers.ContainsKey(subscriber))
                    {
                        subscribers[subscriber] += 1;
                    }
                    else
                    {
                        subscribers[subscriber] = 1;
                    }
                }
                outputSubscribersToMultilineTextbox(subscribers);

                GC.Collect();
                GC.WaitForPendingFinalizers();

                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);

                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);

                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
            }
        }

        private string formatSubscriptionString(string p)
        {
            string[] removes = { "<ParameterValues>", "</ParameterValues>", "<ParameterValue>", "</ParameterValue>", "<Name>", "<Value>" };
            foreach (string rem in removes)
            {
                p = p.Replace(rem, "");
            }
            p = p.Replace("</Name>", "&");
            p = p.Replace("</Value>", "^");

            return p.Remove(p.Length - 1);
        }

        private void outputSubscribersToMultilineTextbox(Dictionary<string, int> subscribers)
        {
            bool first = true;
            foreach(KeyValuePair<string, int> subscriber in subscribers)
            {
                if (first)
                {
                    txtShowSubscribers.Text = subscriber.Key + " has " + subscriber.Value.ToString() + " subscriptions";
                    first = false;
                }
                else
                {
                    txtShowSubscribers.AppendText("\r\n" + subscriber.Key + " has " + subscriber.Value.ToString() + " subscriptions");
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ofdOpen.Reset();
            lblChosenFile.Text = "";
            txtShowSubscribers.Clear();
        }
    }
}
