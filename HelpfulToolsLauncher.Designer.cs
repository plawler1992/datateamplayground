﻿namespace HelpfulToolsLauncher
{
    partial class Launcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnParameterGenerator = new System.Windows.Forms.Button();
            this.btnStoredProcUsage = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnRFL = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnParameterGenerator
            // 
            this.btnParameterGenerator.Location = new System.Drawing.Point(12, 12);
            this.btnParameterGenerator.Name = "btnParameterGenerator";
            this.btnParameterGenerator.Size = new System.Drawing.Size(225, 78);
            this.btnParameterGenerator.TabIndex = 0;
            this.btnParameterGenerator.Text = "Parameter Generator";
            this.btnParameterGenerator.UseVisualStyleBackColor = true;
            this.btnParameterGenerator.Click += new System.EventHandler(this.btnParameterGenerator_Click);
            // 
            // btnStoredProcUsage
            // 
            this.btnStoredProcUsage.Location = new System.Drawing.Point(12, 97);
            this.btnStoredProcUsage.Name = "btnStoredProcUsage";
            this.btnStoredProcUsage.Size = new System.Drawing.Size(225, 80);
            this.btnStoredProcUsage.TabIndex = 1;
            this.btnStoredProcUsage.Text = "Find Stored Proc Usage";
            this.btnStoredProcUsage.UseVisualStyleBackColor = true;
            this.btnStoredProcUsage.Click += new System.EventHandler(this.btnStoredProcUsage_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(11, 266);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(224, 76);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnRFL
            // 
            this.btnRFL.Location = new System.Drawing.Point(12, 183);
            this.btnRFL.Name = "btnRFL";
            this.btnRFL.Size = new System.Drawing.Size(223, 77);
            this.btnRFL.TabIndex = 5;
            this.btnRFL.Text = "RFL";
            this.btnRFL.UseVisualStyleBackColor = true;
            this.btnRFL.Click += new System.EventHandler(this.btnRFL_Click);
            // 
            // Launcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(249, 353);
            this.Controls.Add(this.btnRFL);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnStoredProcUsage);
            this.Controls.Add(this.btnParameterGenerator);
            this.Name = "Launcher";
            this.Text = "Helpful Tools Launcher";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnParameterGenerator;
        private System.Windows.Forms.Button btnStoredProcUsage;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnRFL;
    }
}

