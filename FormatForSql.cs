﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
TODO: Replace single quotes with two single quotes
*/

namespace HelpfulToolsLauncher
{
    public partial class FormatForSql : Form
    {
        string fileName = String.Empty;

        public FormatForSql()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(ofdOpen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lblCSVFile.Text = ofdOpen.FileName;
                fileName = ofdOpen.FileName;
            }
        }

        private void btnDisplayOutput_Click(object sender, EventArgs e)
        {
            List<String> lines = new List<String>();

            if (rbtnValues.Checked)
            {
                using(var reader = new StreamReader(fileName))
                {
                    while (!reader.EndOfStream)
                    {
                        bool first = true;
                        String line = String.Empty;
                        String[] values = reader.ReadLine().Split(',');
                        foreach (String value in values)
                        {
                            String v = value;
                            if(v.ToUpper() != "NULL")
                            {
                                v = "'" + v + "'";
                            }

                            if (first)
                            {
                                line = "(" + v;
                                first = false;
                            }
                            else
                            {
                                line = line + "," + v;
                            }
                        }
                        //line.Remove(line.Length - 2);
                        line = line + ")";
                        lines.Add(line);
                    }
                }
            }

            outputListToMultilineTextbox(lines);
        }

        private void outputListToMultilineTextbox(List<String> lst)
        {
            bool first = true;
            foreach(String l in lst)
            {
                if (first)
                {
                    txtSQL.Text = l;
                    first = false;
                }
                else
                {
                    txtSQL.AppendText("\r\n ," + l);
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ofdOpen.Reset();
            lblCSVFile.Text = "";
            txtSQL.Clear();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtSQL.Text);
        }
    }
}
