﻿namespace HelpfulToolsLauncher
{
    partial class RFL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRFLFile = new System.Windows.Forms.Button();
            this.lblRFLFile = new System.Windows.Forms.Label();
            this.ofdRFLFile = new System.Windows.Forms.OpenFileDialog();
            this.btnCreateRFL = new System.Windows.Forms.Button();
            this.sfdRFL = new System.Windows.Forms.SaveFileDialog();
            this.lblEnterYourName = new System.Windows.Forms.Label();
            this.txtEnterName = new System.Windows.Forms.TextBox();
            this.lblSavedTo = new System.Windows.Forms.Label();
            this.lblIncidentNumber = new System.Windows.Forms.Label();
            this.txtIncidentNumber = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnRFLFile
            // 
            this.btnRFLFile.Location = new System.Drawing.Point(13, 13);
            this.btnRFLFile.Name = "btnRFLFile";
            this.btnRFLFile.Size = new System.Drawing.Size(75, 23);
            this.btnRFLFile.TabIndex = 0;
            this.btnRFLFile.Text = "Choose File";
            this.btnRFLFile.UseVisualStyleBackColor = true;
            this.btnRFLFile.Click += new System.EventHandler(this.btnRFLFile_Click);
            // 
            // lblRFLFile
            // 
            this.lblRFLFile.AutoSize = true;
            this.lblRFLFile.Location = new System.Drawing.Point(94, 18);
            this.lblRFLFile.Name = "lblRFLFile";
            this.lblRFLFile.Size = new System.Drawing.Size(0, 13);
            this.lblRFLFile.TabIndex = 2;
            // 
            // btnCreateRFL
            // 
            this.btnCreateRFL.Location = new System.Drawing.Point(254, 67);
            this.btnCreateRFL.Name = "btnCreateRFL";
            this.btnCreateRFL.Size = new System.Drawing.Size(113, 22);
            this.btnCreateRFL.TabIndex = 3;
            this.btnCreateRFL.Text = "Create RFL Script";
            this.btnCreateRFL.UseVisualStyleBackColor = true;
            this.btnCreateRFL.Click += new System.EventHandler(this.btnCreateRFL_Click);
            // 
            // lblEnterYourName
            // 
            this.lblEnterYourName.AutoSize = true;
            this.lblEnterYourName.Location = new System.Drawing.Point(10, 43);
            this.lblEnterYourName.Name = "lblEnterYourName";
            this.lblEnterYourName.Size = new System.Drawing.Size(87, 13);
            this.lblEnterYourName.TabIndex = 4;
            this.lblEnterYourName.Text = "Enter your name:";
            // 
            // txtEnterName
            // 
            this.txtEnterName.Location = new System.Drawing.Point(105, 40);
            this.txtEnterName.Name = "txtEnterName";
            this.txtEnterName.Size = new System.Drawing.Size(143, 20);
            this.txtEnterName.TabIndex = 5;
            // 
            // lblSavedTo
            // 
            this.lblSavedTo.AutoSize = true;
            this.lblSavedTo.Location = new System.Drawing.Point(13, 91);
            this.lblSavedTo.Name = "lblSavedTo";
            this.lblSavedTo.Size = new System.Drawing.Size(0, 13);
            this.lblSavedTo.TabIndex = 6;
            // 
            // lblIncidentNumber
            // 
            this.lblIncidentNumber.AutoSize = true;
            this.lblIncidentNumber.Location = new System.Drawing.Point(13, 72);
            this.lblIncidentNumber.Name = "lblIncidentNumber";
            this.lblIncidentNumber.Size = new System.Drawing.Size(88, 13);
            this.lblIncidentNumber.TabIndex = 7;
            this.lblIncidentNumber.Text = "Incident Number:";
            // 
            // txtIncidentNumber
            // 
            this.txtIncidentNumber.Location = new System.Drawing.Point(107, 69);
            this.txtIncidentNumber.Name = "txtIncidentNumber";
            this.txtIncidentNumber.Size = new System.Drawing.Size(141, 20);
            this.txtIncidentNumber.TabIndex = 8;
            // 
            // RFL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 113);
            this.Controls.Add(this.txtIncidentNumber);
            this.Controls.Add(this.lblIncidentNumber);
            this.Controls.Add(this.lblSavedTo);
            this.Controls.Add(this.txtEnterName);
            this.Controls.Add(this.lblEnterYourName);
            this.Controls.Add(this.btnCreateRFL);
            this.Controls.Add(this.lblRFLFile);
            this.Controls.Add(this.btnRFLFile);
            this.Name = "RFL";
            this.Text = "RFL";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRFLFile;
        private System.Windows.Forms.Label lblRFLFile;
        private System.Windows.Forms.OpenFileDialog ofdRFLFile;
        private System.Windows.Forms.Button btnCreateRFL;
        private System.Windows.Forms.SaveFileDialog sfdRFL;
        private System.Windows.Forms.Label lblEnterYourName;
        private System.Windows.Forms.TextBox txtEnterName;
        private System.Windows.Forms.Label lblSavedTo;
        private System.Windows.Forms.Label lblIncidentNumber;
        private System.Windows.Forms.TextBox txtIncidentNumber;
    }
}