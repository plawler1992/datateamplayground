﻿namespace FindStoredProcUsage
{
    partial class FindStoredProcUsage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblStoredProc = new System.Windows.Forms.Label();
            this.txtStoredProc = new System.Windows.Forms.TextBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtOutputFiles = new System.Windows.Forms.TextBox();
            this.chkAll = new System.Windows.Forms.CheckBox();
            this.lblExtensions = new System.Windows.Forms.Label();
            this.chkSQL = new System.Windows.Forms.CheckBox();
            this.chkRDL = new System.Windows.Forms.CheckBox();
            this.chkText = new System.Windows.Forms.CheckBox();
            this.btnChoosePath = new System.Windows.Forms.Button();
            this.lblPath = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.fbdChoosePath = new System.Windows.Forms.FolderBrowserDialog();
            this.btnCopyOutput = new System.Windows.Forms.Button();
            this.chkSearchSubDirectories = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblStoredProc
            // 
            this.lblStoredProc.AutoSize = true;
            this.lblStoredProc.Location = new System.Drawing.Point(9, 69);
            this.lblStoredProc.Name = "lblStoredProc";
            this.lblStoredProc.Size = new System.Drawing.Size(63, 13);
            this.lblStoredProc.TabIndex = 2;
            this.lblStoredProc.Text = "Stored Proc";
            // 
            // txtStoredProc
            // 
            this.txtStoredProc.Location = new System.Drawing.Point(79, 69);
            this.txtStoredProc.Name = "txtStoredProc";
            this.txtStoredProc.Size = new System.Drawing.Size(362, 20);
            this.txtStoredProc.TabIndex = 3;
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(447, 36);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(66, 52);
            this.btnFind.TabIndex = 4;
            this.btnFind.Text = "Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(590, 36);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(58, 53);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtOutputFiles
            // 
            this.txtOutputFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutputFiles.Location = new System.Drawing.Point(12, 111);
            this.txtOutputFiles.Multiline = true;
            this.txtOutputFiles.Name = "txtOutputFiles";
            this.txtOutputFiles.Size = new System.Drawing.Size(636, 388);
            this.txtOutputFiles.TabIndex = 6;
            // 
            // chkAll
            // 
            this.chkAll.AutoSize = true;
            this.chkAll.Location = new System.Drawing.Point(76, 12);
            this.chkAll.Name = "chkAll";
            this.chkAll.Size = new System.Drawing.Size(37, 17);
            this.chkAll.TabIndex = 7;
            this.chkAll.Text = "All";
            this.chkAll.UseVisualStyleBackColor = true;
            this.chkAll.CheckedChanged += new System.EventHandler(this.chkAll_CheckedChanged);
            // 
            // lblExtensions
            // 
            this.lblExtensions.AutoSize = true;
            this.lblExtensions.Location = new System.Drawing.Point(12, 12);
            this.lblExtensions.Name = "lblExtensions";
            this.lblExtensions.Size = new System.Drawing.Size(58, 13);
            this.lblExtensions.TabIndex = 8;
            this.lblExtensions.Text = "Extensions";
            // 
            // chkSQL
            // 
            this.chkSQL.AutoSize = true;
            this.chkSQL.Location = new System.Drawing.Point(120, 12);
            this.chkSQL.Name = "chkSQL";
            this.chkSQL.Size = new System.Drawing.Size(42, 17);
            this.chkSQL.TabIndex = 9;
            this.chkSQL.Text = ".sql";
            this.chkSQL.UseVisualStyleBackColor = true;
            this.chkSQL.CheckedChanged += new System.EventHandler(this.chkSQL_CheckedChanged);
            // 
            // chkRDL
            // 
            this.chkRDL.AutoSize = true;
            this.chkRDL.Location = new System.Drawing.Point(169, 12);
            this.chkRDL.Name = "chkRDL";
            this.chkRDL.Size = new System.Drawing.Size(40, 17);
            this.chkRDL.TabIndex = 10;
            this.chkRDL.Text = ".rdl";
            this.chkRDL.UseVisualStyleBackColor = true;
            this.chkRDL.CheckedChanged += new System.EventHandler(this.chkRDL_CheckedChanged);
            // 
            // chkText
            // 
            this.chkText.AutoSize = true;
            this.chkText.Location = new System.Drawing.Point(216, 12);
            this.chkText.Name = "chkText";
            this.chkText.Size = new System.Drawing.Size(40, 17);
            this.chkText.TabIndex = 11;
            this.chkText.Text = ".txt";
            this.chkText.UseVisualStyleBackColor = true;
            this.chkText.CheckedChanged += new System.EventHandler(this.chkText_CheckedChanged);
            // 
            // btnChoosePath
            // 
            this.btnChoosePath.Location = new System.Drawing.Point(12, 36);
            this.btnChoosePath.Name = "btnChoosePath";
            this.btnChoosePath.Size = new System.Drawing.Size(101, 23);
            this.btnChoosePath.TabIndex = 12;
            this.btnChoosePath.Text = "Choose Path";
            this.btnChoosePath.UseVisualStyleBackColor = true;
            this.btnChoosePath.Click += new System.EventHandler(this.btnChoosePath_Click);
            // 
            // lblPath
            // 
            this.lblPath.AutoSize = true;
            this.lblPath.Location = new System.Drawing.Point(119, 41);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(0, 13);
            this.lblPath.TabIndex = 13;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnCopyOutput
            // 
            this.btnCopyOutput.Location = new System.Drawing.Point(519, 36);
            this.btnCopyOutput.Name = "btnCopyOutput";
            this.btnCopyOutput.Size = new System.Drawing.Size(65, 52);
            this.btnCopyOutput.TabIndex = 14;
            this.btnCopyOutput.Text = "Copy Output";
            this.btnCopyOutput.UseVisualStyleBackColor = true;
            this.btnCopyOutput.Click += new System.EventHandler(this.btnCopyOutput_Click);
            // 
            // chkSearchSubDirectories
            // 
            this.chkSearchSubDirectories.AutoSize = true;
            this.chkSearchSubDirectories.Location = new System.Drawing.Point(447, 7);
            this.chkSearchSubDirectories.Name = "chkSearchSubDirectories";
            this.chkSearchSubDirectories.Size = new System.Drawing.Size(135, 17);
            this.chkSearchSubDirectories.TabIndex = 15;
            this.chkSearchSubDirectories.Text = "Search Sub-Directories";
            this.chkSearchSubDirectories.UseVisualStyleBackColor = true;
            // 
            // FindStoredProcUsage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 511);
            this.Controls.Add(this.chkSearchSubDirectories);
            this.Controls.Add(this.btnCopyOutput);
            this.Controls.Add(this.lblPath);
            this.Controls.Add(this.btnChoosePath);
            this.Controls.Add(this.chkText);
            this.Controls.Add(this.chkRDL);
            this.Controls.Add(this.chkSQL);
            this.Controls.Add(this.lblExtensions);
            this.Controls.Add(this.chkAll);
            this.Controls.Add(this.txtOutputFiles);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.txtStoredProc);
            this.Controls.Add(this.lblStoredProc);
            this.Name = "FindStoredProcUsage";
            this.Text = "Find Stored Proc Usage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblStoredProc;
        private System.Windows.Forms.TextBox txtStoredProc;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox txtOutputFiles;
        private System.Windows.Forms.CheckBox chkAll;
        private System.Windows.Forms.Label lblExtensions;
        private System.Windows.Forms.CheckBox chkSQL;
        private System.Windows.Forms.CheckBox chkRDL;
        private System.Windows.Forms.CheckBox chkText;
        private System.Windows.Forms.Button btnChoosePath;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog fbdChoosePath;
        private System.Windows.Forms.Button btnCopyOutput;
        private System.Windows.Forms.CheckBox chkSearchSubDirectories;
    }
}

