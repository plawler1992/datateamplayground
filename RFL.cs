﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace HelpfulToolsLauncher
{
    public partial class RFL : Form
    {
        string sFileName;

        public RFL()
        {
            sFileName = String.Empty;
            InitializeComponent();
        }

        private void btnRFLFile_Click(object sender, EventArgs e)
        {
            if(ofdRFLFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lblRFLFile.Text = ofdRFLFile.FileName;
                sFileName = ofdRFLFile.FileName;
            }
        }

        private void btnCreateRFL_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook;
            Excel.Worksheet xlSheet;
            List<string> lstLines = new List<string>();

            xlWorkbook = xlApp.Workbooks.Open(sFileName);
            xlSheet = (Excel.Worksheet)xlWorkbook.Worksheets.Item[1];

            foreach(Excel.Range row in xlSheet.Rows)
            {
                string sLine = string.Empty;
                if (row.Cells[1][1].Value == null)
                {
                    break;
                }

                for (int i = 1; i < 13; i++)
                {
                    Excel.Range cell = row.Cells[i][1];
                    if (cell.Value == null)
                    {
                        sLine += "NULL,";
                    }
                    else if(cell.Value.ToString() == "NULL")
                    {
                        sLine += cell.Value + ",";
                    }
                    else
                    {
                        sLine += "'" + cell.Value + "',";
                    }
                }
                sLine = sLine.Remove(sLine.Count() - 1);
                lstLines.Add(sLine);
            }

            sfdRFL.Filter = "txt files (*.txt)|*.txt|sql files (*.sql)|*.sql";
            sfdRFL.FilterIndex = 2;
            if (sfdRFL.ShowDialog() == DialogResult.OK)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(sfdRFL.FileName))
                {
                    file.WriteLine(getSQLScript(lstLines));
                }
                lblSavedTo.Text = "File saved to: " + sfdRFL.FileName;
            }
        }

        private string getSQLScript(List<string> lstLines)
        {
            string sSQL = string.Empty;
            string sNL = Environment.NewLine;
            string sValues = string.Empty;

            sValues = "    (" + lstLines[1] + ")" + sNL;
            for(int i = 2; i < lstLines.Count(); i++)
            {
                sValues += "    , (" + lstLines[i] + ")" + sNL;
            }            

            //Comment Header
            sSQL = "/*" + sNL + sNL +
                "Incident - " + formatIncidentNumber() + sNL +
                "Server - DB02vprd" + sNL +
                "Created By: " + txtEnterName.Text + sNL +
                "Created Date: " + DateTime.Today.ToString("MM/dd/yyyy") + sNL + sNL +
                "*/";

            //Initial Message
            sSQL += sNL + sNL + "USE [EchoOptimizer];" + sNL +
                "---------------------------------" + sNL +
                "DECLARE @INITIAL INT;" + sNL +
                "DECLARE @CURRENT INT;" + sNL +
                "DECLARE @Message1 VARCHAR(35);" + sNL +
                "SET @Message1=CHAR(13)+'-----IMPORT-----';" + sNL +
                "PRINT @Message1;" +sNL +
                "SET @INITIAL=(SELECT COUNT(*)FROM [EchoOptimizer].[dbo].[tmp_SalesClass]);" + sNL +
                "SET @Message1=CAST(@INITIAL AS VARCHAR) + ' Records Found';" + sNL +
                "PRINT @Message1;" + sNL +
                "SET @Message1=CHAR(13)+'Importing New...';" + sNL +
                "PRINT @Message1;" + sNL +
                "--------------------------------";

            //Test import into table variable
            sSQL += sNL + sNL +
                "--Test Import to table variable" + sNL +
                "--DECLARE @New_SalesClass AS TABLE([SalesClass] [NVARCHAR](255) NULL," + sNL +
                "--[UserName] [NVARCHAR](255) NULL," + sNL +
                "--[FullName] [NVARCHAR](255) NULL," + sNL +
                "--[LiveDate] [DATETIME] NULL," + sNL +
                "--[FirstFullMonth] [DATETIME] NULL," + sNL +
                "--[RepOrAgent] [NVARCHAR](255) NULL," + sNL +
                "--[Tier] [INT] NULL," + sNL +
                "--[Tier_1] [DATETIME] NULL" + sNL +
                "--[Tier_2] [DATETIME] NULL" + sNL +
                "--[Tier_3] [DATETIME] NULL" + sNL +
                "--[Tier_4] [DATETIME] NULL" + sNL +
                "--[Tier_5] [DATETIME] NULL);";

            //Insert into the table
            sSQL += sNL +
                "BEGIN TRY" + sNL +
                "    BEGIN TRANSACTION;" + sNL + sNL +
                "    --INSERT INTO @New_SalesClass" + sNL +
                "    --'SalesClass','UserName','FullName','LiveDate','FirstFullMonth','RepOrAgent','Tier','Tier_1','Tier_2','Tier_3','Tier_4','Tier_5'" + sNL +
                "    INSERT INTO [EchoOptimizer].[dbo].[tmp_SalesClass]" + sNL +
                "    VALUES" + sNL +
                sValues + 
                "    COMMIT TRANSACTION;" + sNL +
                "END TRY" + sNL +
                "BEGIN CATCH" + sNL +
                "    SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState, ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage;" + sNL +
                "    ROLLBACK TRANSACTION;" + sNL +
                "END CATCH";

            //Final Message
            sSQL += sNL + sNL +
                "--------------------------------" + sNL +
                "--SELECT * FROM @New_SalesClass" + sNL +
                "--SELECT *" + sNL +
                "--FROM [EchoOptimizer].[dbo].[tmp_SalesClass];" + sNL +
                "SET @CURRENT=(SELECT COUNT(*) FROM [EchoOptimizer].[dbo].[tmp_SalesClass]);" + sNL +
                "SET @Message1=CHAR(13)+CAST(@CURRENT AS VARCHAR)+' Records Found';" + sNL +
                "PRINT @Message1;" + sNL +
                "SET @Message1=CAST(@CURRENT-@INITIAL AS VARCHAR)+' Added';" + sNL +
                "PRINT @Message1;" + sNL +
                "--------------------------------";

            return sSQL;
        }

        private string formatIncidentNumber()
        {
            if(txtIncidentNumber.Text.Substring(0,4).ToUpper() == "INC0")
            {
                return txtIncidentNumber.Text;
            }
            else
            {
                return "INC0" + txtIncidentNumber.Text;
            }
        }
    }
}
