﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParameterGenerator
{
    public partial class ParameterGenerator : Form
    {
        private const string SUBSCRIPTION_COPY_MESSAGE = "Copy from ReportsServer.tblSubscriptions Extension Settings or Parameters";
        private const string REPORTS_COPY_MESSAGE = "Copy from ReportsServer.ExecutionLog2 Parameters";

        public ParameterGenerator()
        {
            InitializeComponent();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            string p = txtParametersIn.Text;
            txtParamOut.Text = "";
            if (rbtnSubscription.Checked)
            {
                p = formatSubscriptionString(p);
                outputSubscriptionString(p);
            }
            else
            {
                outputReportString(p);
            }
        }

        private void outputSubscriptionString(string p)
        {
            Dictionary<string, string> lines = new Dictionary<string, string>();
            foreach (string l in p.Split('^'))
            {
                string key = "";
                string val = "";
                if (l.Contains('&'))
                {
                    key = l.Split('&')[0];
                    val = l.Split('&')[1];
                }
                else
                {
                    key = l;
                }

                if (lines.ContainsKey(key))
                {
                    lines[key] = lines[key] + val + ",";
                }
                else
                {
                    lines[key] = val + ",";
                }
            }

            outputSubscriptionToMultilineTextbox(lines);
        }

        private void outputReportString(string p)
        {
            Dictionary<string, string> lines = new Dictionary<string, string>();
            foreach(string l in p.Split('&'))
            {
                string key = l.Split('=')[0];
                string val = l.Split('=')[1];
                if(lines.ContainsKey(key))
                {
                    lines[key] = lines[key] + val + ",";
                }
                else
                {
                    lines[key] = val + ",";
                }
            }
            outputReportToMultilineTextbox(lines);
        }

        private void outputSubscriptionToMultilineTextbox(Dictionary<string, string> lines)
        {
            bool first = true;
            foreach (KeyValuePair<string, string> item in lines)
            {
                if (first)
                {
                    txtParamOut.Text = item.Key + ": " + item.Value.Remove(item.Value.Length - 1);
                    first = false;
                }
                else
                {
                    txtParamOut.AppendText("\r\n" + item.Key + ": " + item.Value.Remove(item.Value.Length - 1));
                }
            }
        }

        private void outputReportToMultilineTextbox(Dictionary<string, string> lines)
        {
            bool first = true;
            foreach (KeyValuePair<string, string> item in lines)
            {
                if (first)
                {
                    if (chkQuotes.Checked)
                    {
                        txtParamOut.Text = "@" + item.Key + " = '" + item.Value.Remove(item.Value.Length - 1) + "',";
                    }
                    else
                    {
                        txtParamOut.Text = "@" + item.Key + " = " + item.Value.Remove(item.Value.Length - 1) + ",";
                    }
                    first = false;
                }
                else
                {
                    if (chkQuotes.Checked)
                    {
                        txtParamOut.AppendText("\r\n@" + item.Key + " = '" + item.Value.Remove(item.Value.Length - 1) + "',");
                    }
                    else
                    {
                        txtParamOut.AppendText("\r\n@" + item.Key + " = " + item.Value.Remove(item.Value.Length - 1) + ",");
                    }
                }
            }
        }

        private string formatSubscriptionString(string p)
        {
            string[] removes = { "<ParameterValues>", "</ParameterValues>", "<ParameterValue>", "</ParameterValue>", "<Name>", "<Value>" };
            foreach (string rem in removes)
            {
                p = p.Replace(rem, "");
            }
            p = p.Replace("</Name>", "&");
            p = p.Replace("</Value>", "^");

            return p.Remove(p.Length - 1);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtParametersIn.Text = "";
            txtParamOut.Text = "";
        }

        private void rbtnReport_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnReport.Checked)
            {
                lblCopyFrom.Text = REPORTS_COPY_MESSAGE;
            }
            else
            {
                lblCopyFrom.Text = SUBSCRIPTION_COPY_MESSAGE;
            }
        }

        private void rbtnSubscription_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnReport.Checked)
            {
                lblCopyFrom.Text = REPORTS_COPY_MESSAGE;
            }
            else
            {
                lblCopyFrom.Text = SUBSCRIPTION_COPY_MESSAGE;
            }
        }
    }
}
