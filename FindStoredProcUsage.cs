﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FindStoredProcUsage
{
    public partial class FindStoredProcUsage : Form
    {
        string sSearchPath = String.Empty;

        public FindStoredProcUsage()
        {
            InitializeComponent();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            lblPath.Text = "";
            fbdChoosePath.Reset();
            txtOutputFiles.Text = "";
            txtStoredProc.Text = "";
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            txtOutputFiles.Text = "";
            string path = sSearchPath;
            string findme = txtStoredProc.Text.ToLower();
            List<string> files = new List<string>();
            List<string> fileTypes = setFileTypes();
            List<string> found_files = new List<string>();

            if (Directory.Exists(path))
            {
                foreach(string ft in fileTypes)
                {
                    files.AddRange(Directory.GetFiles(path, ft, chkSearchSubDirectories.Checked ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));
                }
                foreach (string file in files)
                {
                    using (StreamReader sr = new StreamReader(file))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            if (line.ToLower().Contains(findme))
                            {
                                found_files.Add(file);
                                break;
                            }
                        }
                    }
                }
                outputToMultilineTextbox(found_files);
            }
            else
            {
                //do something invalid path, or empty stuff
            }
        }

        private List<string> setFileTypes()
        {
            List<string> ret = new List<string>();
            if (chkAll.Checked)
            {
                ret.Add("*.*");
            }
            else
            {
                if (chkSQL.Checked) { ret.Add("*.sql"); }
                if (chkRDL.Checked) { ret.Add("*.rdl"); }
                if (chkText.Checked) { ret.Add("*.txt"); }
            }
            if (ret.Count == 0)
            {
                    ret.Add("*.*");
            }
            return ret;
        }

        private void outputToMultilineTextbox(List<string> outputFiles)
        {
            if (outputFiles.Count == 0)
            {
                txtOutputFiles.AppendText("No results found");
            }
            else
            {
                bool first = true;
                foreach (string output in outputFiles)
                {
                    if (first)
                    {
                        txtOutputFiles.AppendText(output + "\n");
                        first = false;
                    }
                    else
                    {
                        txtOutputFiles.AppendText("\r\n\n" + output + "\n");
                    }
                }
            }
        }

        private void chkAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAll.Checked)
            {
                foreach(CheckBox cb in this.Controls.OfType<CheckBox>())
                {
                    if (!cb.Equals(sender)) { cb.Checked = false; }
                }
            }
        }

        private void chkSQL_CheckedChanged(object sender, EventArgs e)
        {
            if(chkSQL.Checked && chkAll.Checked)
            {
                chkAll.Checked = false;
            }
        }

        private void chkRDL_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRDL.Checked && chkAll.Checked)
            {
                chkAll.Checked = false;
            }
        }

        private void chkText_CheckedChanged(object sender, EventArgs e)
        {
            if (chkText.Checked && chkAll.Checked)
            {
                chkAll.Checked = false;
            }
        }

        private void btnChoosePath_Click(object sender, EventArgs e)
        {
            if(fbdChoosePath.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lblPath.Text = fbdChoosePath.SelectedPath;
                sSearchPath = fbdChoosePath.SelectedPath;
            }
        }

        private void btnCopyOutput_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtOutputFiles.Text);
        }
    }
}
