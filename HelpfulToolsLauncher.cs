﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ParameterGenerator;
using FindStoredProcUsage;


namespace HelpfulToolsLauncher
{
    public partial class Launcher : Form
    {
        //private const string PARAMETER_GENERATOR = "ParameterGenerator";
        //private const string FIND_STORED_PROC_USAGE = "FindStoredProcUsage";
        //private string param_path = "C:/Users/plawler/Documents/Visual Studio 2015/Projects/ConsoleApplication2/FindStoredProcUsage/bin/Debug";
        //private string find_path = ""
        public Launcher()
        {
            InitializeComponent();
            //do this programatically later
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnParameterGenerator_Click(object sender, EventArgs e)
        {
            ParameterGenerator.ParameterGenerator frmParameterGenerator = new ParameterGenerator.ParameterGenerator();
            frmParameterGenerator.Show();
        }

        private void btnStoredProcUsage_Click(object sender, EventArgs e)
        {
            FindStoredProcUsage.FindStoredProcUsage frmFindStoredProcUsage = new FindStoredProcUsage.FindStoredProcUsage();
            frmFindStoredProcUsage.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ShowSubscribers frmShowSubscribers = new ShowSubscribers();
            frmShowSubscribers.Show();
        }

        private void btnFormatForSQL_Click(object sender, EventArgs e)
        {
            FormatForSql frmFormatForSql = new FormatForSql();
            frmFormatForSql.Show();
        }

        private void btnRFL_Click(object sender, EventArgs e)
        {
            RFL frmRFL = new RFL();
            frmRFL.Show();
        }
    }
}
